using System;
using System.Collections.Generic;
using Microsoft.EntityFrameworkCore;

namespace MonopConsole.Data;

public class MonopContext : DbContext
{
    public DbSet<Game> Games { get; set; }
    public DbSet<User> Users { get; set; }

    public string DbPath { get; }

    public MonopContext()
    {
        var folder = Environment.SpecialFolder.LocalApplicationData;
        var path = Environment.GetFolderPath(folder);
        DbPath = Path.Join(path, "monop_games_store.db");
    }

    // The following configures EF to create a Sqlite database file in the
    // special "local" folder for your platform.
    protected override void OnConfiguring(DbContextOptionsBuilder options)
        => options.UseSqlite($"Data Source={DbPath}");
}

public class Game
{
    public int Id { get; set; }
    public string Title { get; set; }
    public DateTime CreatedAt { get; set; }
    public bool Finished { get; set; }
    public string GameState { get; set; }

    public ICollection<User> Users { get; set; }
}

public class User
{
    public int Id { get; set; }
    public string Name { get; set; }
    public string TGName { get; set; }
    public string AboutInfo { get; set; }
    public string UserRank { get; set; }
    public bool IsBot { get; set; }
    public DateTime AddedAt { get; set; }
    public ICollection<Game> Games { get; set; }
}