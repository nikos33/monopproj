﻿using MonopConsole;
using MonopLib;
using MonopLib.Managers;

class TestClass
{
    static void Main(string[] args)
    {
        string cmd;
        string playerName = "JonHuman";

        Game g = NewGame(new[] { "JonHuman", "AshotBot" });
        do
        {
            //MapPrinter.PrintGameInfo2(g);

            var roundText = ShowGameState(g, g.CurrPlayer.Name);
            if (!g.CurrPlayer.IsBot)
                Console.WriteLine(roundText);

            cmd = Console.ReadLine();

            if (GameHelper.IsValidCommand(g, cmd))
            {
                ProcessCommand(g, cmd, playerName);
            }

        } while (cmd != "q");


    }

    static Game NewGame(string[] botNames)
    {
        var newGame = GameHelper.StartGame(botNames);
        return newGame;
    }

    static void Print(Game g, string str) => Console.WriteLine($"[{g.State}]---{str}");

    static string ShowGameState(Game g, string curr)
    {
        string infoText = g.State switch
        {
            GameState.BeginStep => string.Format("start round, {0}", g.Config.IsManualRollMode ? "choose one number [1..6]" : "write [game roll]"),
            GameState.CanBuy => $"you can buy #{g.CurrCell.Title} or auction, write [game b] or [game a]",
            GameState.Auction => "do you want bid? [y n]",
            GameState.Trade => $"player #{g.CurrTradeBox.From.Id} wants trade, give #{string.Join(",                                            ", g.CurrTradeBox.GiveCells)} wants #{string.Join(", ", g.CurrTradeBox.GetCells)}, write [game y] or [game n]",
            GameState.CantPay => "you need mortgage cells to find money",
            GameState.NeedPay => "yoy need pay, write [p]",
            GameState.RandomCell => "RandomCell",
            GameState.MoveToCell => "press 'go' to proceed new position",
            GameState.EndStep => "press any key to  finish round or #p to show methods trace",
            _ => "unknown game state"
        };
        return infoText;
    }

    static void ProcessCommand(Game g, string cmd, string curr)
    {
        switch (g.State)
        {
            case GameState.BeginStep:
                if (cmd.StartsWith("m"))
                    Mortgage(g, cmd);
                if (cmd.StartsWith("um"))
                    UnMortgage(g, cmd);

                g.CheckRollAndMakeStep();
                break;
            case GameState.CanBuy:
                if (cmd == "b" || string.IsNullOrEmpty(cmd))
                    PlayerManager.Buy(g);
                else if (cmd == "a")
                    g.ToAuction();
                else if (cmd.StartsWith("m"))
                    Mortgage(g, cmd);
                else if (cmd.StartsWith("um"))
                    UnMortgage(g, cmd);

                break;

            case GameState.Auction:
                g.AuctionStrategy.RunActionJob(cmd);
                break;

            case GameState.Trade:
                if (cmd != "y")
                    TradeManager.CompleteTrade(g);
                else
                    TradeManager.AddToRejectedTrades(g);
                break;

            case GameState.CantPay or GameState.NeedPay:
                PlayerManager.Pay(g);
                break;

            case GameState.RandomCell:
                g.FinishStep("");
                break;

            case GameState.MoveToCell:
                PlayerStepsManager.MoveAfterRandom(g);
                break;

            case GameState.EndStep:
                if (cmd == "p")
                    PrintMethodTrace(g);
                else
                    g.FinishGameRound();
                break;

            default:
                //Console.WriteLine(g.State.ToString());
                break;
        }
    }

    private static void PrintMethodTrace(Game g)
    {
        var trace = string.Join("\\n", g.MethodsTrace);
        g.MethodsTrace.ForEach(l => Console.WriteLine(l));
    }

    static void UnMortgage(Game g, string cmd)
    {
        throw new NotImplementedException();
    }

    static void Mortgage(Game g, string cmd)
    {
        throw new NotImplementedException();
    }

}
