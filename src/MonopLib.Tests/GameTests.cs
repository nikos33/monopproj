using System.Linq;
using System;
using System.Collections.Generic;
using System.Diagnostics;
using MonopLib.Managers;
using Xunit;

namespace MonopLib.Tests;

public class GameTests
{
    public GameTests()
    {
    }

    [Fact]
    public void AuctionTest()
    {
        var pl1Data = new PlayerTestData { Pid = 1, Pos = 9, MyMoney = 1000, MyCells = new() { 11, 13, 15 } };
        var pl2Data = new PlayerTestData { Pid = 2, Pos = 14, MyMoney = 5000, MyCells = new() { 5, 9, 21 } };

        Game g = TestsHelper.SetPlayersAssets(new[] { pl1Data, pl2Data });

        g.LastRollAsInt = 23;
        PlayerStepsManager.MakeStepRoll(g);
        var cells1 = g.Map.CellsByUser(1).Select(item => $"cell{item.Id}-{item.IsMortgage}");
        var result = string.Join(",", cells1);

        Assert.Equal("cell11-True,cell13-False,cell14-False,cell15-False", result);
    }

    string MortgageText(bool isMortg) => isMortg ? "M" : "P";

    [Fact]
    public void TradeTest()
    {
        var pl1Data = new PlayerTestData { Pid = 1, Pos = 8, MyMoney = 6000, MyCells = new() { 6, 8, 14, 16 } };
        var pl2Data = new PlayerTestData { Pid = 2, Pos = 14, MyMoney = 8000, MyCells = new() { 9, 11, 13, 21 } };

        Game g = TestsHelper.SetPlayersAssets(new[] { pl1Data, pl2Data });

        g.LastRollAsInt = 23;
        PlayerStepsManager.MakeStep(g);

        //Assert.NotNull(g.CurrTradeBox);
        Assert.Equal(false, g.CurrTradeBox.Reversed);

    }
}