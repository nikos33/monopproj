using System.Collections.Generic;

namespace MonopLib.Tests;

public class PlayerTestData
{
    public int Pid { get; internal set; }
    public List<int> MyCells { get; set; }
    public int Pos { get; set; }
    public int MyMoney { get; set; }
}