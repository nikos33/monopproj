using Xunit;

namespace MonopLib.Tests;

public class StepTests
{
    public StepTests()
    {
    }

    [Fact]
    public void Test1()
    {
        Game g = TestsHelper.CreateAndInitGame();
        g.ToFirstRound();
        for (int i = 0; i < 20; i++)
        {
            g.CheckRollAndMakeStep();
        }

    }
}