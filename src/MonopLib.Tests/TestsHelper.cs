using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using MonopLib.Managers;

namespace MonopLib.Tests
{
    public class TestsHelper
    {
        public static Game CreateAndInitGame()
        {
            Game g = new Game(1, "ru");
            var pl1 = new Player(1, "Vitek", PlayerType.Bot, 1500);
            var pl2 = new Player(2, "MegaBot", PlayerType.Bot, 1500);
            g.Players.Add(pl1);
            g.Players.Add(pl2);
            g.State = GameState.BeginStep;

            g.Config.ConfirmRoundEnding = true;
            g.Config.IsConsole = true;

            return g;
        }

        public static Game SetPlayersAssets(PlayerTestData[] data)
        {
            Game g = CreateAndInitGame();
            foreach (var plData in data)
            {
                var pl = g.FindPlayerBy(plData.Pid);
                pl.Money = plData.MyMoney;
                pl.Pos = plData.Pos;
                plData.MyCells.ForEach(c => g.Cells[c].Owner = plData.Pid);
            }
            g.Map.UpdateCellsGroupInfo();

            return g;
        }

        public static Game InitAuctionState(int cellId)
        {
            Game g = CreateAndInitGame();
            return g;
        }


    }
}