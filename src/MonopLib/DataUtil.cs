namespace MonopLib;

internal class DataUtil
{
    static int ParseInt(string arr)
    {
        Int32.TryParse(arr.Trim(), out int result);
        return result;
    }

    internal static void InitCellsAndChestCards(Game g, string folder)
    {
        InitCells(g, folder);
        InitChestCards(g, folder);
    }

    static void InitCells(Game g, string folder)
    {
        var lang = g.UILanguage;
        var lines = File.ReadAllLines(Path.Combine(folder, $"GameData/lands_{lang}.txt"));
        var Cells = new List<Cell>(40);
        foreach (var line in lines.Skip(1))
        {
            if (string.IsNullOrWhiteSpace(line)) continue;

            var arr = line.Split('|');
            var cell = new Cell
            {
                Title = arr[0].Trim(),
                Id = ParseInt(arr[1]),
                Cost = ParseInt(arr[2]),
                Type = ParseInt(arr[3]),
                Group = ParseInt(arr[4]),
                RentInfo = arr[5].Trim(),
                Info = arr[6],
            };
            Cells.Add(cell);
        }
         g.Cells = Cells.OrderBy(c => c.Id).ToList();
    }

    static void InitChestCards(Game g, string folder)
    {
        var lang = g.UILanguage;
        var lines = File.ReadAllLines(Path.Combine(folder, $"GameData/chest_cards_{lang}.txt"));
        var Cards = new List<ChestCard>(30);
        foreach (var line in lines.Skip(1))
        {
            if (string.IsNullOrWhiteSpace(line)) continue;

            var arr = line.Split('|');
            var card = new ChestCard
            {
                RandomGroup = ParseInt(arr[0]),
                Type = ParseInt(arr[1]),
                Text = arr[2].Trim(),
            };
            if (arr.Length > 3)
                card.Money = ParseInt(arr[3]);
            if (arr.Length > 4)
                card.Pos = ParseInt(arr[4]);
            Cards.Add(card);
        }
        g.CommunityChest = Cards.Where(cr => cr.Type == 1).ToArray();
        g.ChanceChest = Cards.Where(cr => cr.Type == 2).ToArray();
    }

    internal static void InitBotRules(Game g, string folder)
    {
        InitAuctionBotRules(g, folder);
        InitTradeBotRules(g, folder);
    }
    internal static void InitAuctionBotRules(Game g, string folder)
    {
        var lines = File.ReadAllLines(Path.Combine(folder, "GameData/auc_rules.txt"));
        foreach (var line in lines.Skip(1))
        {
            if (string.IsNullOrWhiteSpace(line)) continue;

            var rule = line.Split(';').ToDictionary(k => k.Split('=').First(), x => x.Split('=').Last());
            var arule = new ARule
            {
                groupId = ParseInt(rule["gid"]),
                myCount = ParseInt(rule["myc"]),
                anCount = ParseInt(rule["anc"]),
                myMoney = ParseInt(rule["money"]),
                housesGroups = rule["nb"],
                factor = Convert.ToDouble(rule["fac"])
            };
            g.BotAuctionRules.Add(arule);
        }
    }

    internal static void InitTradeBotRules(Game g, string folder)
    {
        var lines = File.ReadAllLines(Path.Combine(folder, "GameData/trade_rules.txt"));
        int id = 0;
        foreach (var line in lines.Skip(1))
        {
            if (string.IsNullOrWhiteSpace(line)) continue;

            var ruleParts = line.Split(';');
            var mm1 = ruleParts[0].Split("-");
            var mm2 = ruleParts[1].Split("-");
            var trule = new TRule
            {
                getLand = ParseInt(mm1[0]),
                getCount = ParseInt(mm1[1]),
                myCount = ParseInt(mm1[2]),

                giveLand = ParseInt(mm2[0]),
                giveCount = ParseInt(mm2[1]),
                yourCount = ParseInt(mm2[2]),
            };
            trule.id = id++;
            if (ruleParts.Length > 2)
            {
                var mm3 = ruleParts[2].Split("-");
                trule.getMoney = ParseInt(mm3[0]);
                trule.giveMoney = ParseInt(mm3[1]);
            }
            trule.moneyFactor = ruleParts.Length > 3 ? Convert.ToDouble(ruleParts[3]) : 1;
            if (ruleParts.Length > 4)
                trule.disabled = ruleParts[4].Trim() == "d=1";
            if (!trule.disabled)
                g.BotTradeRules.Add(trule);
        }
    }
}
