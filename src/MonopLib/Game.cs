using System.Reflection;
using MonopLib.Managers;

namespace MonopLib;

public class Game
{
    //metainfo
    public int Id { get; set; }
    public int Round { get; set; }
    public int LastRollAsInt { get; set; }
    public int ManualRoll { get; set; }
    public GameConfig Config { get; set; }
    public GameState State { get; set; }
    public string UILanguage { get; set; }

    //player and cells info
    public List<Player> Players { get; set; }
    public List<Cell> Cells { get; set; }
    public int Selected { get; set; }
    public int SelectedPos { get; set; }
    public int[][] PlayerCellGroups { get; set; } = new[] { new int[12], new int[12], new int[12], new int[12], new int[12] };

    //pay info
    public int PayAmount { get; set; }
    public int? PayToUser { get; set; }

    //Chest map cards
    public ChestCard[] CommunityChest { get; set; }
    public ChestCard[] ChanceChest { get; set; }
    public ChestCard LastRandomCard { get; set; }

    //Auction
    public Auction CurrAuction { get; set; }
    public List<ARule> BotAuctionRules { get; set; } = new(100);
    //Trading
    public TradeBox CurrTradeBox { get; set; }
    public List<TradeBox> CompletedTrades { get; set; } = new(30);
    public List<TradeBox> RejectedTrades { get; set; } = new(30);
    public List<TRule> BotTradeRules { get; set; } = new(30);
    public List<TRule> PlayerTradeRules(int pid) => BotTradeRules;

    public Map Map { get; set; }

    //managers
    public IAuctionStrategy AuctionStrategy { get; set; }


    public Game(int Id, string Lang)
    {
        this.Id = Id;
        UILanguage = Lang;
        Config = new() { NeedShowLog = true, IsManualRollMode = false };
        Round = 0;
        Selected = 0;
        Players = new(4);
        DataUtil.InitCellsAndChestCards(this, GameDataFolder);
        DataUtil.InitBotRules(this, GameDataFolder);
        State = GameState.Start;
        Map = new Map(this);
        //managers
        AuctionStrategy = new StandartAuctionStrategy(this);
    }

    public void SetConfirmActionAfterFinishingRound()
    {
        this.Config.ConfirmRoundEnding = true;
    }

    public string GameDataFolder => Path.GetDirectoryName(Assembly.GetExecutingAssembly().Location);

    public Player CurrPlayer => Players[Selected];
    public Cell CurrCell => Cells[CurrPlayer.Pos];
    public Player FindPlayerBy(int pid) => Players.Find(pl => pl.Id == pid);
    public (int r1, int r2) LastRoll => (LastRollAsInt / 10, LastRollAsInt % 10);

    //Logs
    private List<string> LabelsLog { get; set; } = new(1000); //xlog
    public List<string> RoundMessages { get; set; } = new(10);
    public List<string> MethodsTrace { get; set; } = new(10);

    public void Logx(string message) => LabelsLog.Add(message);
    public void AddRoundMessage(string ru_text, string en_text) => LogRoundMessage(BuildMessage(ru_text, en_text));
    public void AddRoundMessage(string text) => LogRoundMessage(text);

    public void AddRoundMessageByLabel(string label, params object[] args) =>
        LogRoundMessage(string.Format(TextRes.Get(UILanguage, label), args));

    private void LogRoundMessage(string text) =>
        //RoundMessages.Add($"{Round} {text}");
        Console.WriteLine($"{Round} {text}");


    internal string BuildMessage(string ru_text, string en_text)
    {
        return UILanguage == "ru" ? ru_text : en_text;
    }

    public void CheckRollAndMakeStep()
    {
        bool needRoll = true;
        if (Config.IsManualRollMode)
            needRoll = Players.All(pl => (pl.IsHuman ? pl.ManualRoll != 0 : true));
        if (needRoll)
            PlayerStepsManager.MakeStep(this);
    }

    public void FinishStep(string act)
    {
        if (!string.IsNullOrWhiteSpace(act))
            Logx(act);

        State = GameState.EndStep;
        if (CurrPlayer.IsHuman && !Config.ConfirmRoundEnding)
            FinishGameRound();
    }

    public static int[] DOUBLE_ROLLS = new[] { 11, 22, 33, 44, 55, 66 };

    public void FinishGameRound()
    {
        //Console.WriteLine(string.Join(Environment.NewLine, this.RoundMessages));
        RoundMessages.Clear();
        MethodsTrace.Clear();

        if (State != GameState.EndStep) return;

        if (CurrPlayer.IsBot)
            GameManager.BotActionsWhenFinishStep(this);

        Logx("_round_finished");
        Console.WriteLine("-------------------------");

        if (Config.IsManualRollMode)
            Players.ForEach(pl => pl.ManualRoll = 0);

        Round++;

        if (!DOUBLE_ROLLS.Contains(LastRollAsInt)) Selected++;

        if (Selected >= Players.Count()) Selected = 0;

        State = GameState.BeginStep;
        CurrPlayer.UpdateTimer();
        if (CurrPlayer.IsBot)
            CheckRollAndMakeStep();
    }

    public void ToFirstRound(int updateInterval = 200)
    {
        State = GameState.BeginStep;
        Round = 1;
        Config.UpdateInterval = updateInterval;
    }

    public void MoveToCell()
    {
        if (CurrPlayer.IsBot)
            PlayerStepsManager.MoveAfterRandom(this);
        else
            State = GameState.MoveToCell;
    }

    public void ToBeginState()
    {
        State = GameState.BeginStep;
    }

    public void FinishAfterChestCard()
    {
        if (CurrPlayer.IsBot)
            PlayerStepsManager.MoveAfterRandom(this);
        else
            State = GameState.MoveToCell;
    }

    public void ToPay(bool FinishStep = true)
    {
        MethodsTrace.Add($"[ToPay]");
        State = GameState.NeedPay;
        if (CurrPlayer.IsBot)
            PlayerManager.Pay(this, FinishStep);
    }

    public void ToPayAmount(int amount, bool FinishStep = true)
    {
        MethodsTrace.Add($"[ToPayAmount] amount:{amount}");

        PayAmount = amount;
        ToPay(FinishStep);
    }

    internal void ToCanBuy()
    {
        MethodsTrace.Add($"[ToCanBuy]");

        State = GameState.CanBuy;
        /// UpdateGame call this method
        // if (CurrPlayer.IsBot)
        //     PlayerManager.Buy(this);
    }

    internal void ToCantPay()
    {
        MethodsTrace.Add($"[ToCantPay]");
        /// UpdateGame call this method
        // if (CurrPlayer.IsBot)
        //     PlayerLeaveGame();
    }

    public void ToAuction()
    {
        State = GameState.Auction;
        AuctionStrategy.InitAuction();
    }

    internal int CalcPlayerAssets(int pid, bool includeMonop = true)
    {
        int sum = 0;
        foreach (var cell in Cells.Where(c => c.IsActive && c.Owner == pid))
        {
            if (includeMonop)
            {
                sum += cell.MortgageAmount;
                sum += cell.HousesCount * cell.HouseCostWhenSell;
            }
            else
            if (!cell.IsMonopoly)
                sum += cell.MortgageAmount;
        }
        sum += FindPlayerBy(pid).Money;
        return sum;
    }

    public void PlayerLeaveGame()
    {
        var pl = CurrPlayer;
        if (Players.Count() >= 2)
        {
            Players.Remove(pl);
            foreach (var cell in Map.CellsByUser(pl.Id))
            {
                cell.Owner = null;
                cell.HousesCount = 0;
            }
            PlayerCellGroups[pl.Id] = new int[12];
            AddRoundMessage($"{pl.Name} покинул игру", $"{pl.Name} left game");
        }

        if (Players.Count() == 1)
        {
            State = GameState.FinishGame;
            AddRoundMessageByLabel("_game_is_finished");
        }
    }
}
