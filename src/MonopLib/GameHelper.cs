namespace MonopLib;

public class GameHelper
{
    //static Timer LifeTimer;

    public static Game StartGame(string[] playerNames)
    {
        Game g = new Game(1, "ru");
        g.Players.AddRange(
            playerNames.Select((nm, idx) => new Player(idx, nm,
             nm.EndsWith("Bot") ? PlayerType.Bot : PlayerType.Human, 15000)));

        g.SetConfirmActionAfterFinishingRound();
        g.Config.IsConsole = true;

        //PrintGame(g);
        GameManager.StartGameAsBackgroundThread(g);
        return g;

    }

    public static bool IsValidCommand(Game g, string cmd)
    {
        if (g.State == GameState.MoveToCell || g.State == GameState.CanBuy || g.State == GameState.EndStep)
            return true;
        if (string.IsNullOrEmpty(cmd))
            return false;
        return true;
    }


    public static Game StartGameInController(string[] botNames) => StartGame(botNames);

    internal static Game CreateGame(int gid)
    {
        var g = new Game(gid, "ru");
        g.SetConfirmActionAfterFinishingRound();
        return g;
    }

}
