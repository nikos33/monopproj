using MonopLib.BotBrains;
using MonopLib.Managers;

namespace MonopLib;

public class GameManager
{
    static Timer LifeTimer;

    public static void StartGame(Game g)
    {
        g.ToFirstRound(1000);
        while (true)
        {
            Thread.Sleep(100);
            GameManager.UpdateGame(g);
        }
    }

    public static void StartGameAsBackgroundThread(Game g)
    {
        g.ToFirstRound(1000);
        LifeTimer = new Timer(GameManager.UpdateGame, g, 0, g.Config.UpdateInterval);
    }

    public static void UpdateGame(Object obj)
    {
        var g = (Game)obj;
        //g.MethodsTrace.Add($"[UpdateGame] {g.State}");

        switch (g.State)
        {
            case GameState.BeginStep:
                g.CheckRollAndMakeStep();
                break;
            case GameState.CanBuy:
                if (g.CurrPlayer.IsBot)
                    PlayerManager.Buy(g);
                break;
            case GameState.Auction:
                g.AuctionStrategy.RunActionJob("auto");
                break;
            case GameState.Trade:
                TradeManager.RunTradeJob(g);
                break;
            case GameState.CantPay or GameState.NeedPay:
                if (g.CurrPlayer.IsBot)
                    g.PlayerLeaveGame();
                break;
            // case GameState.MoveToCell:
            //     TradeManager.RunTradeJob(g);
            //     break;
            case GameState.EndStep:
                //if (g.CurrPlayer.IsBot || !g.Config.ConfirmRoundEnding)
                if (!g.Config.ConfirmRoundEnding)
                    g.FinishGameRound();
                break;

            default:
                break;
        }
    }

    public static bool BotActionsBeforeRoll(Game g)
    {
        if (TradeLogic.TryDoTrade(g))
            TradeManager.RunTradeJob(g);
        return false;
    }

    public static void BotActionsWhenFinishStep(Game g)
    {
        CellsLogic.UnmortgageSell(g);
        var sum = 0.8 * g.CalcPlayerAssets(g.CurrPlayer.Id, false);
        HousesLogic.BuildHouses(g, (int)sum);
    }
}