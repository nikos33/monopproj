using MonopLib.BotBrains;

namespace MonopLib.Managers;

public class PlayerManager
{
    public static bool Pay(Game g, bool finishRound = true)
    {
        var p = g.CurrPlayer;
        p.UpdateTimer();
        var amount = g.PayAmount;
        bool ok = p.IsBot ? CellsLogic.MortgageSell(g, p, amount) : p.Money >= amount;
        if (ok)
        {
            p.Money -= amount;
            if (p.Police > 0)
            {
                p.Police = 0;
                g.Logx("_paid_500_and_go_from_jail");
                g.AddRoundMessageByLabel("_player_paid_to_exit_from_jail", p.Name);
                PlayerStepsManager.ChangePosAndProcessPosition(g);
                return true;
            }
            else if (g.PayToUser.HasValue)
            {
                var pl = g.FindPlayerBy(g.PayToUser.Value);
                pl.Money += amount;
                g.AddRoundMessageByLabel("_player_paid_to_user", g.CurrPlayer.Name, amount, pl.Name);
                g.PayToUser = default;
            }
            else
                g.AddRoundMessageByLabel("_player_paid", g.CurrPlayer.Name, amount);

            if (finishRound)
                g.FinishStep("_paid ${amount}");
            else
                g.State = GameState.BeginStep;

            g.PayAmount = 0;
            return true;
        }
        else
        {
            g.Logx("_not_enough_money");
            g.ToCantPay();
            return false;
        }
    }

    public static void Buy(Game g)
    {
        if (g.State != GameState.CanBuy) return;

        var p = g.CurrPlayer;
        p.UpdateTimer();
        var cell = g.CurrCell;
        if (cell.Land && !cell.Owner.HasValue)
        {
            if (p.IsBot)
            {
                var ff = BuyLogic.FactorOfBuy(g, p, cell);
                bool needBuy = ff >= 1;
                if (ff == 1 && p.Money < cell.Cost)
                    needBuy = false;
                else if (ff > 1 && p.Money < cell.Cost)
                    needBuy = CellsLogic.MortgageSell(g, p, cell.Cost);

                if (needBuy)
                {
                    g.Map.SetOwner(p, cell, cell.Cost);
                    g.AddRoundMessageByLabel("_player_bought_cell", cell.Title, cell.Cost);
                    g.FinishStep($"_bought #{cell.Title}");
                }
                else
                    g.ToAuction();
            }
            else
            {
                if (p.Money < cell.Cost)
                {
                    g.State = GameState.CanBuy;
                    g.AddRoundMessageByLabel("_not_enough_money");
                }
                else
                {
                    g.Map.SetOwner(p, cell, cell.Cost);
                    g.AddRoundMessageByLabel("_player_bought_cell", cell.Title, cell.Cost);
                    g.FinishStep($"_bought #{cell.Title}");
                }
            }
        }
    }

    public string GoMortgageCells(Game g, Player pl, int[] cidArr)
    {
        string res = "";
        foreach (var cid in cidArr)
        {
            var cell = g.Cells[cid];
            if (cell.IsMortgage || cell.HousesCount > 0) continue;
            pl.Money += cell.MortgageAmount;
            cell.IsMortgage = true;
            res += $"_{cid}";
        }
        return res;
    }

    public string GoUnmortgageCells(Game g, Player pl, int[] cidArr)
    {
        string res = "";
        foreach (var cid in cidArr)
        {
            var cell = g.Cells[cid];
            if (cell.IsMortgage)
            {
                pl.Money -= cell.UnMortgageAmount;
                cell.IsMortgage = false;
                res += $"_{cid}";
            }
        }
        return res;
    }
    public string GoBuildHouses(Game g, Player pl, int[] cidArr)
    {
        string res = "";
        foreach (var cid in cidArr)
        {
            var cell = g.Cells[cid];
            if (cell.IsMonopoly && cell.HousesCount < 4)
            {
                cell.HousesCount++;
                pl.Money -= cell.HouseCost;
                res += $"_{cid}";
            }
        }
        return res;
    }

    public string GoSellHouses(Game g, Player pl, int[] cidArr)
    {
        string res = "";
        foreach (var cid in cidArr)
        {
            var cell = g.Cells[cid];
            if (cell.HousesCount > 0)
            {
                cell.HousesCount--;
                pl.Money += cell.HouseCostWhenSell;
                res += $"_{cid}";
            }
        }
        return res;
    }
}
