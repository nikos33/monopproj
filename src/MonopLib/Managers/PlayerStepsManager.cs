namespace MonopLib.Managers;

public class PlayerStepsManager
{
    static Random _currRandom = new Random();

    public static void MakeStep(Game g)
    {
        if (g.State != GameState.BeginStep) return;

        if (g.CurrPlayer.IsBot &&
            GameManager.BotActionsBeforeRoll(g))
            return;

        g.CurrPlayer.UpdateTimer();
        int r1, r2;

        if (!g.Config.IsManualRollMode)
        {
            r1 = 1 + _currRandom.Next(6);
            r2 = 1 + _currRandom.Next(6);
            g.LastRollAsInt = r1 * 10 + r2;
            if (r1 == r2) g.CurrPlayer.DoubleRoll++;
            else
                g.CurrPlayer.DoubleRoll = 0;
        }
        else
        {
            g.Players.ForEach(pl =>
               {
                   if (pl.IsBot) pl.ManualRoll = _currRandom.Next(6) + 1;
               });
            int sum = 0;
            g.Players.ForEach(pl => { if (pl.Id != g.CurrPlayer.Id) sum += pl.ManualRoll; });
            r1 = g.CurrPlayer.ManualRoll;
            r2 = sum != 0 ? (int)Math.Floor((double)sum / (g.Players.Count() - 1)) : _currRandom.Next(6) + 1;
        }
        g.MethodsTrace.Add($"[MakeStep] rolled {g.LastRollAsInt}");
        MakeStepRoll(g);
        // g.LogRounds.Add(new() { Round = g.Round, Pid = g.Selected, Roll = g.LastRoll });
    }

    public static void MakeStepRoll(Game g)
    {
        //if (g.State != GameState.BeginStep) return;

        var result = Step(g);
        var curr = g.CurrPlayer;
        if (result == "go")
        {
            var oldPos = curr.Pos;
            curr.MoveToNewPos(g.LastRollAsInt);
            g.AddRoundMessageByLabel("_you_visisted_cell", $"#({oldPos}->#{curr.Pos})", g.CurrCell.Title);

            ProcessPosition(g);
        }
        else
            g.FinishStep(result);

    }

    static string Step(Game g)
    {
        var pl = g.CurrPlayer;
        (int r1, int r2) = g.LastRoll;

        var last = pl.Pos;
        string beforeRollText = "";

        if (pl.IsBot && pl.Police > 0 && CalcJailExit(g))
        {
            pl.Money -= 500;
            pl.Police = 0;
            beforeRollText = g.BuildMessage("заплатил $500 чтобы выйти из тюрьмы", "you paid $500 to exit from jail")
                + Environment.NewLine;
            g.Logx("_paid_500_and_go_from_jail");
        }

        var rolls = $"({r1},{r2})";
        var plInfo = $"{pl.Name}(money: ${pl.Money})";

        var endText = beforeRollText + string.Format(TextRes.Get(g.UILanguage, "_player_rolled"), plInfo, rolls);
        g.AddRoundMessage(endText);
        if (pl.Police > 0)
        {
            if (r1 == r2)
            {
                g.AddRoundMessage("вы выходите из тюрьмы по дублю", "you exit from jail because of double roll");
                pl.Police = 0;
            }
            else
            {
                pl.Police += 1;
                if (pl.Police == 4)
                {
                    g.AddRoundMessage("вы должны заплатить $500 чтобы выйти из тюрьмы", "you must pay $500 to go from jail");
                    g.ToPayAmount(500, false);
                    return "_pay500_go";
                }
                else
                {
                    g.AddRoundMessage("вы пропускаете ход в тюрьме", "you passed turn");
                    return "_police_:not_roll_doudle";
                }

            }
        }
        if (CheckOnTripple(pl.Rolls))
        {
            pl.Pos = 10;
            pl.Police = 1;
            pl.Rolls.Add(0);
            return "_tripple_roll";
        }
        return "go";
    }

    static bool CalcJailExit(Game g)
    {
        return true;
    }

    public static bool CheckOnTripple(List<int> steps)
    {
        if (steps.Count() > 2)
            return steps.TakeLast(3).All(ss => Game.DOUBLE_ROLLS.Contains(ss));
        return false;
    }

    public static void ProcessPosition(Game g)
    {
        var p = g.CurrPlayer;
        var cell = g.CurrCell;
        if (cell.Land)
            ProcessLand(g, p, cell);

        else if (cell.Type == 6) // tax cells
            g.ToPayAmount(cell.Rent());

        else if (cell.Type == 4) // Chest cells
            ProcessChestCard(g, p);

        else if (p.Pos == 30)
        {
            p.Pos = 10;
            p.Police = 1;
            g.FinishStep("_go_jail_after_30");
        }
        else
            g.FinishStep("_no_functional_cell");
    }

    private static void ProcessLand(Game g, Player p, Cell cell)
    {
        g.MethodsTrace.Add($"[ProcessLand] cell:{cell.Id}");

        if (!cell.Owner.HasValue)
        {
            g.AddRoundMessage($"Вы можете купить эту землю #{g.CurrCell.Title} за ${g.CurrCell.Cost}", $"You can buy this cell #{g.CurrCell.Title}");
            g.ToCanBuy();
        }
        else if (cell.Owner != p.Id)
        {
            if (cell.IsMortgage)
                g.FinishStep("_cell_mortgaged");
            else
            {
                g.PayToUser = cell.Owner;
                g.AddRoundMessage($"заплатите ренту {cell.Rent()}", $"pay rent $#{cell.Rent()}");
                g.ToPayAmount(cell.Rent());

            }
        }
        else if (cell.Owner == p.Id)
        {
            g.AddRoundMessage($"вы попали на свою землю", $"you visited your own cell");
            g.FinishStep($"_mycell #{cell.Title}");
        }

    }

    // invoke  in Player.Pay()
    public static void ChangePosAndProcessPosition(Game g)
    {
        g.MethodsTrace.Add($"[ChangePosAndProcessPosition] cell:{g.CurrPlayer.Pos}");
        var pl = g.CurrPlayer;
        (int r1, int r2) = g.LastRoll;
        pl.Pos += r1 + r2;
        ProcessPosition(g);
    }

    public static void ProcessChestCard(Game g, Player p)
    {
        g.Map.TakeRandomCard();
        g.MethodsTrace.Add($"[ProcessChestCard] card #{g.LastRandomCard.Text}");

        var card = g.LastRandomCard;
        g.AddRoundMessageByLabel("_random_took_card", card.Text);

        switch (card.RandomGroup)
        {
            //получить мани
            case 1:
                p.Money += card.Money;
                g.FinishAfterChestCard();
                break;
            //заплатить
            case 12:
                g.ToPayAmount(card.Money);
                g.FinishAfterChestCard();
                break;
            case 2 or 3:
                g.MoveToCell();
                break;
            case 4:
                g.PayAmount = card.Money * g.Players.Count();
                g.Players.ForEach(pl => pl.Money += card.Money);
                g.ToPay();
                break;
            case 5:
                p.PoliceKey++;
                g.FinishAfterChestCard();
                break;
            case 15:
                var hh = g.Map.GetHotelsAndHouses(p.Id);
                g.PayAmount = 400 * hh.Item1 + 100 * hh.Item2;
                g.ToPay();
                break;
            default:
                g.FinishStep("finish_unknown_random");
                break;
        }
    }

    public static void MoveAfterRandom(Game g)
    {
        var c = g.LastRandomCard;
        g.MethodsTrace.Add($"[MoveAfterRandom] group:{c.RandomGroup} #{c.Text}");

        var pl = g.CurrPlayer;
        if (c.RandomGroup == 1)
        {
            g.FinishStep("_after_chest_card");
        }
        else if (c.RandomGroup == 2 && c.Pos == 10)
        {
            pl.Pos = 10;
            pl.Police = 1;
            g.AddRoundMessage("мусора вас забрали в тюрьму!", "you went to police jain after chest card");
        }
        else if (c.RandomGroup == 2)
        {
            if (pl.Pos > c.Pos)
            {
                pl.Money += 2000;
                g.AddRoundMessage("вы прошли старт и получили $2000", "you passed start and got $2000");
            }
            pl.Pos = c.Pos;
            ProcessPosition(g);
        }
        else if (c.RandomGroup == 3)
        {
            if (pl.Pos > 3) pl.Pos -= 3;
            ProcessPosition(g);
        }
        else
        {
            //g.AddRoundMessage($"неизвестная карточка {c.Text}", $"undefined card {c.Text}");
            g.FinishStep("_no_move_chest_card_");
        }
    }
}