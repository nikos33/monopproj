﻿using System.Diagnostics;
using Microsoft.AspNetCore.Mvc;
using MonopWeb.Models;
using Microsoft.Extensions.Caching.Memory;
using MonopLib;

namespace MonopWeb.Controllers;

public class GameController : Controller
{
    private readonly ILogger<HomeController> _logger;
    private readonly GameService _gameService;

    public GameController(ILogger<HomeController> logger, GameService gameService)
    {
        _logger = logger;
        _gameService = gameService;
    }

    string CurrentPlayerName => "Monukk";

    public IActionResult Index()
    {
        return View();
    }

    public IActionResult List()
    {
        var model = _gameService.GetGamesList();
        if (model.Count()==0)
            GenerateNewGames();
        return View(model);
    }

    private List<Game> GenerateNewGames(int count = 10)
    {
        for (int i = 1; i < count; i++)
        {
            var gg = BotHelper.CreateGame(i);
            _gameService.AddToGamesList(gg);
        }
        return _gameService.GetGamesList();
    }

    public IActionResult NewGame()
    {
        var plGame = _gameService.FindGameByName(CurrentPlayerName);
        if (plGame == null)
        {
            var gg = BotHelper.StartGame(new[] { CurrentPlayerName, "StupidBot" });
            _gameService.AddToGamesList(gg);
        }
        return RedirectToAction("List");
    }

    [Route("/game/play/{gid}")]
    public IActionResult Play([FromRoute] int gid)
    {
        var gg = _gameService.FindGameByGid(gid);
        if (gg == null)
        {
            RedirectToAction("List");
        }
        return View(gg);
    }

    [Route("/game/join/{gid}")]
    public IActionResult Join([FromRoute] int gid)
    {
        var gg = _gameService.FindGameByGid(gid);
        if (gg != null)
            _gameService.AddPlayer(gg, CurrentPlayerName);
        return RedirectToAction("List");
    }

    [Route("/game/add_bot/{gid}")]
    public IActionResult AddBot([FromRoute] int gid)
    {
        var gg = _gameService.FindGameByGid(gid);
        if (gg != null)
            GameHelper.AddBot(gg);
        return RedirectToAction("List");
    }

    [Route("/game/state/{gid}")]
    public IActionResult State([FromRoute] int gid)
    {
        Game gg = _gameService.FindGameByGid(gid);
        if (gg != null)
        {
            var map = gg.Cells.Select(c => new
            {
                id = c.Id,
                own = c.Owner,
                grcount = c.OwnerGroupCount,
                hh = c.HousesCount,
                text = GameHelper.RenderCellName(c)
            });
            var data = new
            {
                PlayersPos = gg.Players.Select(pl => new { id = pl.Id, pos = pl.Pos }),
                GameLog = string.Join("<br />", gg.RoundMessages),
                PlayersInfo = GameHelper.Info(gg),
                PlayerButton = "",
                Map = map
            };
            return Json(data);
        }
        else
            return BadRequest();
    }
}
