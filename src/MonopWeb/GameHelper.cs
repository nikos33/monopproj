using System.Text;
using MonopLib;

namespace MonopWeb;

public class GameHelper
{
    const string PLAYER_IMG = "<img src='/game/images/p{0}.png' >";

    public static string Info(Game gg)
    {
        StringBuilder result = new();
        result.Append("<table>");
        var data = gg.Players.Select(pl =>
        {
            var plImage = string.Format(PLAYER_IMG, pl.Id);
            return $@"<tr><td>{plImage}</td> <td> <b>{pl.Name}</b> </td>
            <td><span style='color: red'>$ {pl.Money}</span> </td></tr>";
        });
        result.AppendJoin(' ', data);
        result.Append("</table>");
        return result.ToString();
    }

    public static string RenderCellName(Cell c)
    {
        if (c.Land)
            return c.Owner.HasValue && c.IsMortgage ? "MORTG" : c.Rent().ToString();
        return "";
    }


    static string[] BOT_NAMES = { "Shoma", "John", "Pisun" };

    public static void AddBot(Game g)
    {
        var pid = g.Players.Count();
        var rand = new Random();
        var randomIndex = rand.Next(0, BOT_NAMES.Length);
        g.Players.Add(new(pid + 1, BOT_NAMES[randomIndex], PlayerType.Bot, 15000));
    }

}
