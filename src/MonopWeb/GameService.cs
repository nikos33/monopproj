using System.Text;
using Microsoft.Extensions.Caching.Memory;
using MonopLib;

namespace MonopWeb;

public class GameService
{
    private IMemoryCache _cache;
    private const string ALL_GAMES = "all_games";

    public GameService(IMemoryCache cache)
    {
        _cache = cache;
        _cache.Set(ALL_GAMES, new List<Game>(10));
    }


    public List<Game> GetGamesList()
    {
        var list = (List<Game>)_cache.Get(ALL_GAMES);
        if (list == null)
        {
           throw new ArgumentNullException("cache is null");
        }
        return list;
    }

    public Game FindGameByGid(int gid) => GetGamesList().Find(g => g.Id == gid);
    public Game FindGameByName(string plName) => GetGamesList().Find(g => g.Players.Any(pl => pl.Name == plName));

    public void AddToGamesList(Game newGame)
    {
        var list = GetGamesList();
        list.Add(newGame);
    }

    public void AddPlayer(Game g, string plName)
    {
        var pid = g.Players.Count();
        g.Players.Add(new(pid + 1, plName, PlayerType.Human, 15000));
    }

}
