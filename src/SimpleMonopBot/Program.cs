﻿using System.Linq;
using Telegram.Bot;
using Telegram.Bot.Args;
using Telegram.Bot.Types;
using Telegram.Bot.Types.Enums;
using Telegram.Bot.Types.InlineQueryResults;
using Telegram.Bot.Types.ReplyMarkups;
using Microsoft.Extensions.Caching.Memory;
using MonopLib;

namespace SimpleMonopBot;

public static class Program
{
    private static TelegramBotClient Bot;
    static IMemoryCache cache;
    static string playerName;

    public static async Task Main()
    {
        cache = new MemoryCache(new MemoryCacheOptions());

        Bot = new TelegramBotClient(Configuration.BotToken);

        var me = await Bot.GetMeAsync();
        Console.Title = playerName = me.Username;

        Bot.OnMessage += BotOnMessageReceived;
        Bot.OnMessageEdited += BotOnMessageReceived;
        Bot.OnCallbackQuery += BotOnCallbackQueryReceived;
        Bot.OnInlineQuery += BotOnInlineQueryReceived;
        Bot.OnInlineResultChosen += BotOnChosenInlineResultReceived;
        Bot.OnReceiveError += BotOnReceiveError;

        Bot.StartReceiving(Array.Empty<UpdateType>());
        Console.WriteLine($"Start listening for @{me.Username}");

        Console.ReadLine();
        Bot.StopReceiving();
    }

    private static async void BotOnMessageReceived(object sender, MessageEventArgs messageEventArgs)
    {
        var message = messageEventArgs.Message;
        if (message == null || message.Type != MessageType.Text)
            return;
        string cmd = message.Text.Split(' ').First();

        if (new[] { "/ng", "/ls" }.Contains(cmd))
            await HandleAdminCommands(cmd, message);
        else
            await HandleGameCommands(cmd, message);

    }

    private static async Task HandleGameCommands(string cmd, Message message)
    {
        var gg = GetGame();
        if (gg == null)
            await Bot.SendTextMessageAsync(chatId: message.Chat.Id, text: "Create new game with /ng");

        if (cmd == "/info")
        {
            var info = gg.Players.Select(pl => $"{pl.Name}({pl.Pos}) Money: ${pl.Money}")
            .Union(gg.RoundMessages);
            await Bot.SendTextMessageAsync(chatId: message.Chat.Id, text: string.Join(Environment.NewLine, info));
        }

        if (cmd == "/cells")
        {
            var cells = gg.Cells.Select(cc => $"{cc.Id} - {cc.Title.PadRight(30)}");
            await Bot.SendTextMessageAsync(chatId: message.Chat.Id, text: string.Join(Environment.NewLine, cells));
        }
        if (cmd == "/my")
        {
            var myCells = gg.Map.CellsByUser(MyPID()).Select(cc => $"{cc.Id} - {cc.Title.PadRight(30)}| /bh{cc.Id} /sh{cc.Id} /mrtg{cc.Id} /unmrtg{cc.Id}");
            var result = myCells.Any() ? string.Join(Environment.NewLine, myCells) : "no cells";
            await Bot.SendTextMessageAsync(chatId: message.Chat.Id, text: result);
        }

        if (cmd == "/hh")
            await SendInlineKeyboard(message, new()
            {
                { "build_house", "build house" },
                { "sell_house", "sell house" }
            });

        if (cmd == "/trade")
            await SendInlineKeyboard(message, new()
            {
                { "build_house", "build house" },
                { "sell_house", "sell house" }
            });

    }

    public static string CacheKey => playerName + "game";

    public static MonopLib.Game GetGame() => (MonopLib.Game)cache.Get(CacheKey);

    public static int MyPID()
    {
        var gg = (MonopLib.Game)cache.Get(CacheKey);
        return gg.Players.First(p => p.Name == playerName).Id;
    }

    private static async Task HandleAdminCommands(string cmd, Message message)
    {
        if (cmd == "/ng")
        {
            var newGame = GameHelper.StartGame(new[] { playerName, "JonMegabot" });
            var result = cache.Set(CacheKey, newGame);
        }
        await Bot.SendTextMessageAsync(chatId: message.Chat.Id, text: "you created game");
    }

    // Send inline keyboard
    // You can process responses in BotOnCallbackQueryReceived handler
    static async Task SendInlineKeyboard(Message message, Dictionary<string, string> options)
    {
        //await Bot.SendChatActionAsync(message.Chat.Id, ChatAction.Typing);

        var buttons = options.Select(kk => InlineKeyboardButton.WithCallbackData(kk.Value, kk.Key));
        var inlineKeyboard = new InlineKeyboardMarkup(buttons);
        await Bot.SendTextMessageAsync(
            chatId: message.Chat.Id,
            text: "Choose",
            replyMarkup: inlineKeyboard
        );
    }

    static async Task SendReplyKeyboard(Message message)
    {
        var replyKeyboardMarkup = new ReplyKeyboardMarkup(
            new KeyboardButton[][]
            {
                        new KeyboardButton[] { "1.1", "1.2" },
                        new KeyboardButton[] { "2.1", "2.2" },
            },
            resizeKeyboard: true
        );

        await Bot.SendTextMessageAsync(
            chatId: message.Chat.Id,
            text: "Choose",
            replyMarkup: replyKeyboardMarkup

        );
    }


    // Process Inline Keyboard callback data
    private static async void BotOnCallbackQueryReceived(object sender, CallbackQueryEventArgs callbackQueryEventArgs)
    {
        var callbackQuery = callbackQueryEventArgs.CallbackQuery;

        await Bot.AnswerCallbackQueryAsync(
            callbackQueryId: callbackQuery.Id,
            text: $"Received {callbackQuery.Data}"
        );

        await Bot.SendTextMessageAsync(
            chatId: callbackQuery.Message.Chat.Id,
            text: $"Received {callbackQuery.Data}"
        );
    }

    #region Inline Mode

    private static async void BotOnInlineQueryReceived(object sender, InlineQueryEventArgs inlineQueryEventArgs)
    {
        Console.WriteLine($"Received inline query from: {inlineQueryEventArgs.InlineQuery.From.Id}");

        InlineQueryResultBase[] results = {
                // displayed result
                new InlineQueryResultArticle(
                    id: "3",
                    title: "TgBots",
                    inputMessageContent: new InputTextMessageContent(
                        "hello"
                    )
                )
            };
        await Bot.AnswerInlineQueryAsync(
            inlineQueryId: inlineQueryEventArgs.InlineQuery.Id,
            results: results,
            isPersonal: true,
            cacheTime: 0
        );
    }

    private static void BotOnChosenInlineResultReceived(object sender, ChosenInlineResultEventArgs chosenInlineResultEventArgs)
    {
        Console.WriteLine($"Received inline result: {chosenInlineResultEventArgs.ChosenInlineResult.ResultId}");
    }

    #endregion

    private static void BotOnReceiveError(object sender, ReceiveErrorEventArgs receiveErrorEventArgs)
    {
        Console.WriteLine("Received error: {0} — {1}",
            receiveErrorEventArgs.ApiRequestException.ErrorCode,
            receiveErrorEventArgs.ApiRequestException.Message
        );
    }
}
